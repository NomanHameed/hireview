<div x-data>
        <div wire:ignore wire:key="video-player">
            <video id="myVideo" class="video-js vjs-default-skin mb-5 rounded shadow-lg" playsinline controls>
                <p class="vjs-no-js">
                    To view this video please enable JavaScript, or consider upgrading to a
                    web browser that
                    <a href="https://videojs.com/html5-video-support/" target="_blank">
                    supports HTML5 video.
                    </a>
                </p>
            </video>
        </div>
        @if ($this->video)
        <button
            x-ref="resetButton"
            @click="$wire.call('resetVideo');"
            type="button"
            class="cursor-pointer inine-flex justify-between items-center focus:outline-none border py-2 px-4 rounded-full shadow-sm text-left hover:text-white text-gray-600 hover:bg-red-600 bg-gray-100 font-medium">
            <svg xmlns="http://www.w3.org/2000/svg"
                class="inline-flex flex-shrink-0 w-6 h-6 -mt-1 mr-1" viewBox="0 0 24 24"
                stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                stroke-linejoin="round">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M14.752 11.168l-3.197-2.132A1 1 0 0010 9.87v4.263a1 1 0 001.555.832l3.197-2.132a1 1 0 000-1.664z" />
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
            <span>Record again</span>
        </button>
        @endif
        <button
            @click="player.record().start();"
            type="button"
            class="cursor-pointer inine-flex justify-between items-center focus:outline-none border py-2 px-4 rounded-full shadow-sm text-left text-white hover:text-gray-600 bg-red-600 hover:bg-gray-100 font-medium">
            <svg xmlns="http://www.w3.org/2000/svg"
                class="inline-flex flex-shrink-0 w-6 h-6 -mt-1 mr-1" viewBox="0 0 24 24"
                stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                stroke-linejoin="round">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M14.752 11.168l-3.197-2.132A1 1 0 0010 9.87v4.263a1 1 0 001.555.832l3.197-2.132a1 1 0 000-1.664z" />
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
            Record Video
        </button>
        <div class="mx-auto text-gray-500 text-xs text-center mt-1">
            Tap to record video or <label for="videoInput" type="button" class="cursor-pointer underline">upload a video</label>.
        </div>
        <div
            x-data="{ isUploading: false, progress: 0 }"
            x-on:livewire-upload-start="isUploading = true"
            x-on:livewire-upload-finish="
                isUploading = false;
                $wire.getUploadedVideoUrl()
                    .then(result => {
                        player.deviceButton.hide();
                        player.bigPlayButton.show();
                        player.controlBar.playToggle.show();
                        player.src({'src':result});
                    });
            "
            x-on:livewire-upload-error="isUploading = false"
            x-on:livewire-upload-progress="progress = $event.detail.progress">

            <!-- Video  Input -->
            <input
                x-ref="videoInput"
                name="video"
                id="videoInput"
                accept="video/*"
                class="hidden"
                type="file"
                {{ $attributes }}
            />

            <!-- Progress Bar -->
            <div x-show="isUploading">
                <progress max="100" x-bind:value="progress" class="rounded-full"></progress>
            </div>
        </div>
</div>

@push('scripts')

<script>
const constraints = {
  video: true,
};

const video = document.querySelector("video");

navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
  video.srcObject = stream;
});

function hasGetUserMedia() {
  return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
}
if (hasGetUserMedia()) {
    alert("getUserMedia() is supported by your browser");
} else {
  alert("getUserMedia() is not supported by your browser");
}

</script>
<script src="{{ mix('js/collect-video.js') }}"></script>
<script>
    // user completed recording and stream is available
    player.on('finishRecord', function() {
        // the blob object contains the recorded data that
        // can be downloaded by the user, stored on server etc.
        console.log('inside blade: ', player.recordedData);

        var data = player.recordedData;

        console.log(data)
        // Upload a file:
        @this.upload('video', data, (uploadedFilename) => {
            // Success callback
        }, () => {
            // Error callback.
        }, (event) => {
            console.log(event);
            // Progress callback.
            // event.detail.progress contains a number between 1 and 100 as the upload progresses.
        });
    });

    // user clicked the record button and started recording
    player.on('startRecord', () => {
        console.log('from blade: started recording!');
        $refs.recordButton.show();
    });
</script>
@endpush
