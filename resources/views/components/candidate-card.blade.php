@if (count($candidates))

<ul class="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 mt-4">
    @foreach ($candidates as $candidate)
        <li x-data="{ isOpen: false}" class="col-span-1 flex flex-col bg-white rounded-lg shadow">

            <div x-data="{ open: false }" class="relative flex flex-row justify-between p-3">
                @if ($candidate->application->is_favorite)
                    <x-heroicon-s-star wire:click="favorite({{ $candidate->application->id }})" class="w-8 h-8 text-yellow-400" />
                @else
                    <x-heroicon-o-star wire:click="favorite({{ $candidate->application->id }})" class="w-8 h-8 text-gray-400" />
                @endif
                <x-heroicon-o-dots-vertical @click="open = !open" @click.away="open = false" class="w-5 h-5 text-gray-400 cursor-pointer z-30" />
                <div x-show="open" x-description="Dropdown panel, show/hide based on dropdown state." x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" class="origin-top-right absolute right-0 top-5 mt-5 -mr-1 w-56 rounded-md shadow-lg">
                    <div class="rounded-md bg-white shadow-xs">
                        <div class="py-1">
                            @foreach ($candidate->application->stages() as $key => $stage)
                                <a wire:click="advance({{ $candidate->application->id }}, '{{ $stage }}')" href="#" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900">
                                    {{ $stage }}
                                </a>
                            @endforeach
                            <div class="border-t border-gray-100"></div>
                            <a wire:click="delete({{ $candidate->application->id }})" href="#" class="group flex items-center px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900">
                                <x-heroicon-o-trash class="mr-3 h-5 w-5 text-gray-400 group-hover:text-gray-500 group-focus:text-gray-500" />
                                Delete
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="flex-1 flex flex-col p-4 text-center">
                <div @if($candidate->application->video) @click="isOpen = true" @endif>
                    <img class="w-32 h-32 flex-shrink-0 mx-auto bg-black rounded-full" src="{{ $candidate->photoUrl(128) }}" alt="{{ $candidate->name }}">
                </div>
                @if ($candidate->application->video)
                    <template x-if="isOpen">
                        <div style="background-color: rgba(0, 0, 0, .5);" class="fixed top-0 left-0 w-full h-full flex items-center shadow-lg overflow-y-auto z-50">
                            <div class="container mx-auto lg:px-32 rounded-lg overflow-y-auto shadow-xl">
                                <div class="bg-white rounded">
                                    <div class="modal-body px-8 py-8">
                                        <div class="responsive-container overflow-hidden relative" style="padding-top: 56.25%">
                                            <video class="responsive-iframe absolute top-0 left-0 w-full h-full" controls>
                                                <source src="{{ $candidate->application->video }}" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>
                                        </div>
                                        <div class="mt-5 sm:mt-6">
                                            <span class="flex w-full rounded-md shadow-sm">
                                                <button @click="isOpen = false" type="button"
                                                    class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-indigo-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                                                    Go back to dashboard
                                                </button>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </template>
                @endif
                <h3 class="mt-6 text-gray-900 text-sm leading-5 font-medium">{{ $candidate->name }}</h3>
                <dl class="mt-1 flex-2 flex flex-row mx-auto">
                    <dt class="sr-only">Attachments</dt>
                    @if ($candidate->application->resume)
                    <dd class="text-gray-500 text-sm leading-5 mr-1">
                        <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-red-100 text-red-800">
                            Resume
                        </span>
                    </dd>
                    @endif
                    @if ($candidate->application->video)
                    <dd class="text-gray-500 text-sm leading-5 ml-1">
                        <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-green-100 text-grenn-800">
                            Video
                        </span>
                    </dd>
                    @endif
                </dl>
                <a href="mailto:{{ $candidate->email }}" class="flex-1 inline-flex items-center justify-center py-4 text-sm leading-5 text-gray-700 font-medium hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 transition ease-in-out duration-150">
                    <span class="ml-3">{{ $candidate->email }}</span>
                </a>
                <a href="tel:{{ $candidate->phone }}" class="flex-1 inline-flex items-center justify-center text-sm leading-5 text-gray-700 font-medium hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 transition ease-in-out duration-150">
                    <span class="ml-3">{{ $candidate->phone }}</span>
                </a>
            </div>
            <div class="border-t border-gray-200">
                <div class="-mt-px flex">
                    <div class="w-0 flex-1 flex border-r border-gray-200">
                        <div class="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm leading-5 text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 transition ease-in-out duration-150">
                            <svg wire:click="discard({{ $candidate->application->id }})" class="h-8 w-full @if($candidate->application->deleted_at) text-red-500 @else text-gray-400 @endif hover:text-red-500 inline-block" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14H5.236a2 2 0 01-1.789-2.894l3.5-7A2 2 0 018.736 3h4.018a2 2 0 01.485.06l3.76.94m-7 10v5a2 2 0 002 2h.096c.5 0 .905-.405.905-.904 0-.715.211-1.413.608-2.008L17 13V4m-7 10h2m5-10h2a2 2 0 012 2v6a2 2 0 01-2 2h-2.5" />
                            </svg>
                        </div>
                    </div>
                    <div class="-ml-px w-0 flex-1 flex">
                        <div class="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm leading-5 text-gray-700 font-medium border border-transparent rounded-br-lg hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 transition ease-in-out duration-150">
                            <svg wire:click="advance({{ $candidate->application->id }})" class="h-8 text-gray-400 hover:text-green-500 inline-block" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 10h4.764a2 2 0 011.789 2.894l-3.5 7A2 2 0 0115.263 21h-4.017c-.163 0-.326-.02-.485-.06L7 20m7-10V5a2 2 0 00-2-2h-.095c-.5 0-.905.405-.905.905 0 .714-.211 1.412-.608 2.006L7 11v9m7-10h-2M7 20H5a2 2 0 01-2-2v-6a2 2 0 012-2h2.5" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    @endforeach
</ul>
@else
<div class="rounded-md bg-yellow-50 p-4 mt-4">
  <div class="flex">
    <div class="flex-shrink-0">
      <!-- Heroicon name: exclamation -->
      <svg class="h-5 w-5 text-yellow-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
        <path fill-rule="evenodd" d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
      </svg>
    </div>
    <div class="ml-3">
      <h3 class="text-sm leading-5 font-medium text-yellow-800">
        No applicants in this stage
      </h3>
      <div class="mt-2 text-sm leading-5 text-yellow-700">
        <p>
          You do not currently have any applicants in this stage of the hiring process.
        </p>
      </div>
    </div>
  </div>
</div>
@endif
