<div>
    <div id="pipe-recorder"></div>
</div>


@push('styles')
<link rel="stylesheet" href="//cdn.addpipe.com/2.0/pipe.css">
@endpush


@push('scripts')
<script type="text/javascript" src="//cdn.addpipe.com/2.0/pipe.js"></script>

<script type="text/javascript">
    var pipeParams = {
        size: {
            width: 640,
            height: 510
        },
        qualityurl:"avq/480p.xml",
        accountHash:"3e34239a6704356c491ab4fe6ed05a9c",
        eid:"UZGFnI",
        mrt:120,
        dup:1,
    };
    PipeSDK.insert("pipe-recorder",pipeParams,function(recorderObject){
        recorderObject.btRecordPressed = function(id){
            var args = Array.prototype.slice.call(arguments);
            alert("btRecordPressed("+args.join(', ')+")");
        }

        myRecorderObject.onSaveOk = function(recorderId, streamName, streamDuration, cameraName, micName, audioCodec, videoCodec, fileType, videoId, audioOnly, location){
            var args = Array.prototype.slice.call(arguments);
            console.log("onSaveOk("+args.join(', ')+")");
        }
    });
</script>
@endpush
