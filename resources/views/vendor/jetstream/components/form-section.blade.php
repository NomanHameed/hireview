@props(['submit'])

<div {{ $attributes->merge(['class' => 'md:grid md:grid-cols-3 md:gap-6 shadow overflow-hidden rounded-md px-4 py-5 bg-white sm:p-6']) }}>
    <x-jet-section-title>
        <x-slot name="title">{{ $title }}</x-slot>
        <x-slot name="description">{{ $description }}</x-slot>
    </x-jet-section-title>

    <div class="mt-5 md:mt-0 md:col-span-2">
        <form wire:submit.prevent="{{ $submit }}">
            <div class="">
                <div class="">
                    <div class="grid gap-6">
                        {{ $form }}
                    </div>
                </div>
            </div>
            @if (isset($actions))
                <div class="flex items-center justify-end pt-4 text-right">
                    {{ $actions }}
                </div>
            @endif
        </form>

    </div>

</div>
