<x-jet-form-section submit="updatePaymentMethod">
    <x-slot name="title">
        {{ __('Invoices') }}
    </x-slot>

    <x-slot name="description">
        {{ __('View and download your previous invoices.') }}
    </x-slot>

    <x-slot name="form">
        <div class="bg-white space-y-6">
            <div>
                <h2 id="plan_heading" class="text-lg leading-6 font-medium text-gray-900">Invoices</h2>
            </div>
        </div>
    </x-slot>
</x-jet-form-section>
