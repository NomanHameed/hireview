<x-jet-form-section submit="updatePaymentMethod">
    <x-slot name="title">
        {{ __('Payment Method') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Update your default payment method.') }}
    </x-slot>

    <x-slot name="form">
        <div class="bg-white space-y-6">
            <div>
                <h2 id="plan_heading" class="text-lg leading-6 font-medium text-gray-900">Card info</h2>
            </div>
        </div>
    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Saved.') }}
        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled">
            {{ __('Update your plan') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
