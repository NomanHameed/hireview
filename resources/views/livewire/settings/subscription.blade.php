<x-jet-form-section submit="updateSubscription">
    <x-slot name="title">
        {{ __('Subscription') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Update your TalentEQ subscription.') }}
    </x-slot>

    <x-slot name="form">
        <div class="bg-white space-y-6">
            <div>
                <h2 id="plan_heading" class="text-lg leading-6 font-medium text-gray-900">Plan</h2>
            </div>

            <fieldset x-data="radioGroup()">
                <legend class="sr-only">
                    Pricing plans
                </legend>
                <ul class="relative bg-white rounded-md -space-y-px" x-ref="radiogroup">


                    <li>
                        <div :class="{ 'border-gray-200': !(active === 0), 'bg-cyan-50 border-cyan-200 z-10': active === 0 }"
                            class="relative border rounded-tl-md rounded-tr-md p-4 flex flex-col md:pl-4 md:pr-6 md:grid md:grid-cols-3 bg-cyan-50 border-cyan-200 z-10">
                            <label class="flex items-center text-sm cursor-pointer">
                                <input name="pricing_plan" type="radio" @click="select(0)" @keydown.space="select(0)"
                                    @keydown.arrow-up="onArrowUp(0)" @keydown.arrow-down="onArrowDown(0)"
                                    class="h-4 w-4 text-cyan-500 cursor-pointer focus:ring-gray-900 border-gray-300"
                                    aria-describedby="plan-option-pricing-0 plan-option-limit-0">
                                <span class="ml-3 font-medium text-gray-900">Startup</span>
                            </label>
                            <p id="plan-option-pricing-0" class="ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-center">
                                <span :class="{ 'text-cyan-900': active === 0, 'text-gray-900': !(active === 0) }"
                                    class="font-medium text-cyan-900">$29 / mo</span>
                                <span :class="{ 'text-cyan-700': active === 0, 'text-gray-500': !(active === 0) }"
                                    class="text-cyan-700">($290 / yr)</span>
                            </p>
                            <p id="plan-option-limit-0"
                                :class="{ 'text-cyan-700': active === 0, 'text-gray-500': !(active === 0) }"
                                class="ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-right text-cyan-700">Up to 5 active
                                job postings</p>
                        </div>
                    </li>


                    <li>
                        <div :class="{ 'border-gray-200': !(active === 1), 'bg-cyan-50 border-cyan-200 z-10': active === 1 }"
                            class="relative border p-4 flex flex-col md:pl-4 md:pr-6 md:grid md:grid-cols-3 border-gray-200">
                            <label class="flex items-center text-sm cursor-pointer">
                                <input name="pricing_plan" type="radio" @click="select(1)" @keydown.space="select(1)"
                                    @keydown.arrow-up="onArrowUp(1)" @keydown.arrow-down="onArrowDown(1)"
                                    class="h-4 w-4 text-cyan-500 cursor-pointer focus:ring-gray-900 border-gray-300"
                                    aria-describedby="plan-option-pricing-1 plan-option-limit-1" checked="">
                                <span class="ml-3 font-medium text-gray-900">Business</span>
                            </label>
                            <p id="plan-option-pricing-1" class="ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-center">
                                <span :class="{ 'text-cyan-900': active === 1, 'text-gray-900': !(active === 1) }"
                                    class="font-medium text-gray-900">$99 / mo</span>
                                <span :class="{ 'text-cyan-700': active === 1, 'text-gray-500': !(active === 1) }"
                                    class="text-gray-500">($990 / yr)</span>
                            </p>
                            <p id="plan-option-limit-1"
                                :class="{ 'text-cyan-700': active === 1, 'text-gray-500': !(active === 1) }"
                                class="ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-right text-gray-500">Up to 25 active
                                job postings</p>
                        </div>
                    </li>


                    <li>
                        <div :class="{ 'border-gray-200': !(active === 2), 'bg-cyan-50 border-cyan-200 z-10': active === 2 }"
                            class="relative border rounded-bl-md rounded-br-md p-4 flex flex-col md:pl-4 md:pr-6 md:grid md:grid-cols-3 border-gray-200">
                            <label class="flex items-center text-sm cursor-pointer">
                                <input name="pricing_plan" type="radio" @click="select(2)" @keydown.space="select(2)"
                                    @keydown.arrow-up="onArrowUp(2)" @keydown.arrow-down="onArrowDown(2)"
                                    class="h-4 w-4 text-cyan-500 cursor-pointer focus:ring-gray-900 border-gray-300"
                                    aria-describedby="plan-option-pricing-2 plan-option-limit-2">
                                <span class="ml-3 font-medium text-gray-900">Enterprise</span>
                            </label>
                            <p id="plan-option-pricing-2" class="ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-center">
                                <span :class="{ 'text-cyan-900': active === 2, 'text-gray-900': !(active === 2) }"
                                    class="font-medium text-gray-900">$249 / mo</span>
                                <span :class="{ 'text-cyan-700': active === 2, 'text-gray-500': !(active === 2) }"
                                    class="text-gray-500">($2490 / yr)</span>
                            </p>
                            <p id="plan-option-limit-2"
                                :class="{ 'text-cyan-700': active === 2, 'text-gray-500': !(active === 2) }"
                                class="ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-right text-gray-500">Unlimited active
                                job postings</p>
                        </div>
                    </li>

                </ul>
            </fieldset>

            <div class="flex items-center">
                <button type="button" @click="on = !on" :aria-pressed="on.toString()" aria-pressed="true"
                    aria-labelledby="toggleLabel" x-data="{ on: true }"
                    :class="{ 'bg-gray-200': !on, 'bg-cyan-500': on }"
                    class="relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-900 transition-colors ease-in-out duration-200 bg-cyan-500">
                    <span class="sr-only">Use setting</span>
                    <span aria-hidden="true" :class="{ 'translate-x-5': on, 'translate-x-0': !on }"
                        class="inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200 translate-x-5"></span>
                </button>
                <span id="toggleLabel" class="ml-3">
                    <span class="text-sm font-medium text-gray-900">Annual billing </span>
                    <span class="text-sm text-gray-500">(Save 10%)</span>
                </span>
            </div>
        </div>
    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Saved.') }}
        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled">
            {{ __('Update your plan') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>

@push('scripts')
<script>
    function radioGroup() {
        return {
          active: 0,
          onArrowUp(index) {
            this.select(this.active - 1 < 0 ? this.$refs.radiogroup.children.length - 1 : this.active - 1);
          },
          onArrowDown(index) {
            this.select(this.active + 1 > this.$refs.radiogroup.children.length - 1 ? 0 : this.active + 1);
          },
          select(index) {
            this.active = index;
          },
        };
      }
</script>
@endpush
