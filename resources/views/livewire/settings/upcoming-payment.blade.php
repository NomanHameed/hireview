<x-jet-form-section submit="updatePaymentMethod">
    <x-slot name="title">
        {{ __('Upcoming Payment') }}
    </x-slot>

    <x-slot name="description">
        {{ __('View your upcoming payment information.') }}
    </x-slot>

    <x-slot name="form">
        <div class="bg-white space-y-6">
            <div>
                <h2 id="plan_heading" class="text-lg leading-6 font-medium text-gray-900">Upcoming Payment</h2>
            </div>
        </div>
    </x-slot>
</x-jet-form-section>
