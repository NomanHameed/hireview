<div>
    <h1 class="sr-only">Profile</h1>
    <!-- Main 3 column grid -->
    <div class="grid grid-cols-1 gap-4 items-start lg:grid-cols-3 lg:gap-8">
        <!-- Left column -->
        <div class="grid grid-cols-1 gap-4 lg:col-span-2">
            <!-- Jobs panel -->
            <section aria-labelledby="profile-overview-title">
                <div class="rounded-lg bg-white overflow-hidden shadow">
                    <h2 class="sr-only" id="profile-overview-title">Profile Overview</h2>
                    <div class="bg-white p-6">
                        <div class="sm:flex sm:items-center sm:justify-between">
                            <div class="sm:flex sm:space-x-5">
                                <div class="flex-shrink-0">
                                    <img class="mx-auto h-20 w-20 rounded-full"
                                        src="{{ Auth::user()->profilePhotoUrl }}"
                                        alt="{{ Auth::user()->name }}">
                                </div>
                                <div class="mt-4 text-center sm:mt-0 sm:pt-1 sm:text-left">
                                    <p class="text-sm font-medium text-gray-600">Welcome back,</p>
                                    <p class="text-xl font-bold text-gray-900 sm:text-2xl">{{ Auth::user()->name }}</p>
                                    <p class="text-sm font-medium text-gray-600">{{ Auth::user()->role }}</p>
                                </div>
                            </div>
                            <div class="mt-5 flex justify-center sm:mt-0">
                                <a href="{{ route('profile.show') }}"
                                    class="flex justify-center items-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50">
                                    View profile
                                </a>
                            </div>
                        </div>
                    </div>
                    <div
                        class="border-t border-gray-200 bg-gray-50 grid grid-cols-1 divide-y divide-gray-200 sm:grid-cols-3 sm:divide-y-0 sm:divide-x">

                        <div class="px-6 py-5 text-sm font-medium text-center">
                            <span class="text-gray-900">{{ $totalPublishedJobs }}</span>
                            <span class="text-gray-600">Published jobs</span>
                        </div>

                        <div class="px-6 py-5 text-sm font-medium text-center">
                            <span class="text-gray-900">{{ $totalCandidates }}</span>
                            <span class="text-gray-600">Candidates</span>
                        </div>

                        <div class="px-6 py-5 text-sm font-medium text-center">
                            <span class="text-gray-900">{{ $totalInterviews }}</span>
                            <span class="text-gray-600">Interviews</span>
                        </div>

                    </div>
                </div>
            </section>

            <!-- Actions panel -->
            <section aria-labelledby="quick-links-title">
                <div class="bg-white overflow-hidden rounded-lg sm:shadow">
                    @livewire('jobs.job-list')
                </div>
            </section>
        </div>

        <!-- Right column -->
        <div class="grid grid-cols-1 gap-4">
            <!-- Create job posting form -->
            <section aria-labelledby="create-job-posting-form-title">
                <div class="rounded-lg bg-white overflow-hidden shadow">
                    <div class="p-6">
                        @livewire('jobs.create-job-form')
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
