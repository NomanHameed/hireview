<div>
    <div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 transition-opacity">
                <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <!-- This element is to trick the browser into centering the modal contents. -->
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>​
            <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
                role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                <div class="px-4 py-5 sm:px-6">
                    <form wire:submit.prevent="updateJob" class="space-y-8 divide-y divide-gray-200">
                        <div class="space-y-8 divide-y divide-gray-200">
                            <div>
                                <div>
                                    <h3 class="text-lg leading-6 font-medium text-gray-900">
                                        Edit job posting
                                    </h3>
                                    <p class="mt-1 text-sm text-gray-500">
                                        This information will be used to build your job posting.
                                    </p>
                                </div>

                                <div class="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
                                    <div class="col-span-6">
                                        <x-jet-label for="title" value="{{ __('Job Title') }}" />
                                        <x-jet-input id="title" type="text" class="mt-1 block w-full"
                                            wire:model.defer="title" autofocus />
                                        <x-jet-input-error for="title" class="mt-2" />
                                    </div>

                                    <div class="sm:col-span-6">
                                        <label for="description" class="block text-sm font-medium text-gray-700">
                                            Video Prompt
                                        </label>
                                        <div class="mt-1">
                                            <textarea id="description" name="description" rows="3"
                                                wire:model.defer="description"
                                                placeholder="Write a few sentences about what type of candidate you're looking for..."
                                                class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"></textarea>
                                            <x-jet-input-error for="description" class="mt-2" />
                                        </div>
                                        <p class="mt-2 text-sm text-gray-500">This will be displayed on the video step.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="pt-8">
                                <div>
                                    <h3 class="text-lg leading-6 font-medium text-gray-900">
                                        Custom fields
                                    </h3>
                                    <p class="mt-1 text-sm text-gray-500">
                                        Please specify which fields are mandatory.
                                    </p>
                                </div>

                                <fieldset>
                                    <div class="mt-4 space-y-4">
                                        <div class="relative flex items-start">
                                            <div class="flex items-center h-5">
                                                <input id="require_video" name="requireVideo" type="checkbox"
                                                    wire:model.defer="requireVideo"
                                                    class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                                                    checked>
                                            </div>
                                            <div class="ml-3 text-sm">
                                                <label for="comments" class="font-medium text-gray-700">Video</label>
                                                <p class="text-gray-500">Require video submission with all applications.
                                                </p>
                                                <x-jet-input-error for="require_video" class="mt-2" />
                                            </div>
                                        </div>
                                        <div class="relative flex items-start">
                                            <div class="flex items-center h-5">
                                                <input id="require_resume" name="requireResume" type="checkbox"
                                                    wire:model.defer="requireResume"
                                                    class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
                                                    checked>
                                            </div>
                                            <div class="ml-3 text-sm">
                                                <label for="candidates" class="font-medium text-gray-700">Resume</label>
                                                <p class="text-gray-500">Require resume attachment with all
                                                    applications.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                            </div>
                        </div>

                        <div class="pt-5">
                            <div class="flex justify-end">
                                <button wire:click="$emitUp('closeModal')" type="button"
                                    class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    Cancel
                                </button>
                                <button type="submit"
                                    class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    Update job posting
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
