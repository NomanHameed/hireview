<div>
    <div class="grid grid-cols-1 gap-4 items-start lg:grid-cols-3 lg:gap-8">
        <div class="grid grid-cols-1 gap-4 lg:col-span-2">
            <div class="bg-white overflow-hidden rounded-lg sm:shadow">
                @livewire('jobs.job-list')
            </div>
        </div>
        <div class="grid grid-cols-1 gap-4">
            <div class="bg-white oveflow-hideen rounded-lg sm:shadow">
                <div class="px-4 py-5 sm:px-6">
                    @livewire('jobs.create-job-form')
                </div>
            </div>
        </div>
    </div>
</div>
