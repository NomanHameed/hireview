<div x-data="{ tab: 'applied' }">
    @if ($editingJob)
        @livewire('jobs.edit-job-modal', [ 'jobPosting' => $jobPosting ])
    @endif

    <div class="bg-white overflow-hidden rounded-lg shadow">

        <div class="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
            <div class="-ml-4 -mt-4 flex justify-between items-center flex-wrap sm:flex-nowrap">
                <div class="ml-4 mt-4">
                    <h2 class="text-2xl leading-6 font-medium text-gray-900">
                        {{ ucwords($jobPosting->title) }}
                    </h2>
                    <div class="mt-1 flex flex-row sm:mt-0 sm:flex-wrap space-x-6">
                        <div class="mt-2 flex items-center text-sm leading-5 text-gray-600">
                            @if($jobPosting->published)
                                <x-heroicon-o-badge-check class="-ml-1 mr-1.5 h-5 w-5 text-green-400" />
                                Published
                            @else
                            <x-heroicon-o-exclamation-circle class="-ml-1 mr-1.5 h-5 w-5 text-red-400" />
                                Unpublished
                            @endif
                        </div>
                        <div class="mt-2 flex items-center text-sm leading-5 text-gray-600">
                            @if($jobPosting->require_video)
                                <x-heroicon-o-badge-check class="-ml-1 mr-1.5 h-5 w-5 text-red-400" />
                            @else
                                <x-heroicon-o-exclamation-circle class="-ml-1 mr-1.5 h-5 w-5 text-red-400" />
                            @endif
                            Video
                        </div>
                        <div class="mt-2 flex items-center text-sm leading-5 text-gray-600">
                            @if($jobPosting->require_resume)
                                <x-heroicon-o-badge-check class="-ml-1 mr-1.5 h-5 w-5 text-red-400" />
                            @else
                                <x-heroicon-o-exclamation-circle class="-ml-1 mr-1.5 h-5 w-5 text-red-400" />
                            @endif
                            Resume
                        </div>

                    </div>
                </div>
                <div class="ml-4 mt-4 flex-shrink-0">
                    <button
                        wire:click="editJob"
                        class="relative inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Edit
                    </button>
                    <a
                        href="{{ $jobPosting->applicationUrl() }}"
                        target="_blank"
                        type="button"
                        class="relative inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        View
                    </a>
                    <button
                        wire:click="@if($jobPosting->published) unpublish @else publish @endif"
                        type="button"
                        class="relative inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        @if($jobPosting->published)
                            Unpublish
                        @else
                            Publish
                        @endif
                    </button>
                </div>
            </div>
        </div>
        @if ($jobPosting->candidates->count())
            <div>
                <div>
                    <div class="sm:hidden">
                        <select @change="tab = $event.target.value" aria-label="Selected tab"
                            class="mt-1 form-select block w-full pl-3 pr-10 py-2 text-base leading-6 border-gray-300 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5 transition ease-in-out duration-150">
                            <option value="applied" selected>Applied</option>
                            <option value="phone">Phone Screening</option>
                            <option value="interview">Interview</option>
                            <option value="offer">Offer</option>
                            <option value="hired">Hired</option>
                            <option value="favorites">Favorites</option>
                            <option value="discarded">Discarded</option>
                        </select>
                    </div>
                    <div class="hidden sm:block">
                        <div class="border-b border-gray-200 mt-4">
                            <nav class="mx-4 mb-4 flex justify-between">
                                <div>
                                    <a x-on:click="tab = 'applied'"
                                        x-bind:class="{ 'border-blue-500 text-blue-600': tab === 'applied', 'text-gray-500 hover:border-gray-300 focus:text-gray-700 focus:border-gray-300 focus:outline-none': tab !== 'applied' }"
                                        href="#"
                                        class="whitespace-no-wrap py-4 px-1 border-b-2 border-transparent font-medium text-sm leading-5">
                                        Applied <span
                                            class="inline-block bg-gray-200 rounded-full px-2 text-sm font-semibold text-gray-700 ml-2">{{ $jobPosting->applied()->count() }}</span>
                                    </a>
                                    <a x-on:click="tab = 'phone'"
                                        x-bind:class="{ 'border-blue-500 text-blue-600': tab === 'phone', 'text-gray-500 hover:border-gray-300 focus:text-gray-700 focus:border-gray-300 focus:outline-none': tab !== 'phone' }"
                                        href="#"
                                        class="whitespace-no-wrap ml-8 py-4 px-1 border-b-2 border-transparent font-medium text-sm leading-5">
                                        Phone Screening <span
                                            class="inline-block bg-gray-200 rounded-full px-2 text-sm font-semibold text-gray-700 ml-2">{{ $jobPosting->phoneScreenings()->count() }}</span>
                                    </a>
                                    <a x-on:click="tab = 'interview'"
                                        x-bind:class="{ 'border-blue-500 text-blue-600': tab === 'interview', 'text-gray-500 hover:border-gray-300 focus:text-gray-700 focus:border-gray-300 focus:outline-none': tab !== 'interview' }"
                                        href="#"
                                        class="whitespace-no-wrap ml-8 py-4 px-1 border-b-2 border-transparent font-medium text-sm leading-5">
                                        Interview <span
                                            class="inline-block bg-gray-200 rounded-full px-2 text-sm font-semibold text-gray-700 ml-2">{{ $jobPosting->interviews()->count() }}</span>
                                    </a>
                                    <a x-on:click="tab = 'offer'"
                                        x-bind:class="{ 'border-blue-500 text-blue-600': tab === 'offer', 'text-gray-500 hover:border-gray-300 focus:text-gray-700 focus:border-gray-300 focus:outline-none': tab !== 'offer' }"
                                        href="#"
                                        class="whitespace-no-wrap ml-8 py-4 px-1 border-b-2 border-transparent font-medium text-sm leading-5">
                                        Offer <span
                                            class="inline-block bg-gray-200 rounded-full px-2 text-sm font-semibold text-gray-700 ml-2">{{ $jobPosting->offers()->count() }}</span>
                                    </a>
                                    <a x-on:click="tab = 'hired'"
                                        x-bind:class="{ 'border-blue-500 text-blue-600': tab === 'hired', 'text-gray-500 hover:border-gray-300 focus:text-gray-700 focus:border-gray-300 focus:outline-none': tab !== 'hired' }"
                                        href="#"
                                        class="whitespace-no-wrap ml-8 py-4 px-1 border-b-2 border-transparent font-medium text-sm leading-5">
                                        Hired <span
                                            class="inline-block bg-gray-200 rounded-full px-2 text-sm font-semibold text-gray-700 ml-2">{{ $jobPosting->hired()->count() }}</span>
                                    </a>
                                </div>

                                <div>
                                    <a x-on:click="tab = 'favorites'"
                                        x-bind:class="{ 'border-blue-500 text-blue-600': tab === 'favorites', 'text-gray-500 hover:border-gray-300 focus:text-gray-700 focus:border-gray-300 focus:outline-none': tab !== 'favorites' }"
                                        href="#"
                                        class="whitespace-no-wrap ml-8 py-4 px-1 border-b-2 border-transparent font-medium text-sm leading-5">
                                        Favorites <span
                                            class="inline-block bg-gray-200 rounded-full px-2 text-sm font-semibold text-gray-700 ml-2">{{ $jobPosting->favoriteCandidates()->count() }}</span>
                                    </a>
                                    <a x-on:click="tab = 'discarded'"
                                        x-bind:class="{ 'border-blue-500 text-blue-600': tab === 'discarded', 'text-gray-500 hover:border-gray-300 focus:text-gray-700 focus:border-gray-300 focus:outline-none': tab !== 'discarded' }"
                                        href="#"
                                        class="whitespace-no-wrap ml-8 py-4 px-1 border-b-2  border-transparent font-medium text-sm leading-5">
                                        Discarded <span
                                            class="inline-block bg-gray-200 rounded-full px-2 text-sm font-semibold text-gray-700 ml-2">{{ $jobPosting->discardedCandidates()->count() }}</span>
                                    </a>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="rounded-md bg-yellow-50 m-6 p-4">
                <div class="flex">
                    <div class="flex-shrink-0">
                        <!-- Heroicon name: exclamation -->
                        <svg class="h-5 w-5 text-yellow-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                            fill="currentColor">
                            <path fill-rule="evenodd"
                                d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div class="ml-3">
                        <h3 class="text-sm leading-5 font-medium text-yellow-800">
                            No candidates available
                        </h3>
                        <div class="mt-2 text-sm leading-5 text-yellow-700">
                            <p>
                                Sorry, no candidates have applied to this position.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    @if ($jobPosting->candidates->count())
        <div>
            <section x-show="tab === 'applied'">
                <x-candidate-card :candidates="$jobPosting->applied()->get()" />
                {{-- @include('livewire/jobs/partials/candidates', [ 'candidates' => $jobPosting->applied()->get() ]) --}}
            </section>
            <section x-show="tab === 'phone'">
                <x-candidate-card :candidates="$jobPosting->phoneScreenings()->get()" />
                {{-- @include('livewire/jobs/partials/candidates', [ 'candidates' =>
                $jobPosting->phoneScreenings()->get() ]) --}}
            </section>
            <section x-show="tab === 'interview'">
                <x-candidate-card :candidates="$jobPosting->interviews()->get()" />
                {{-- @include('livewire/jobs/partials/candidates', [ 'candidates' => $jobPosting->interviews()->get() ]) --}}
            </section>
            <section x-show="tab === 'offer'">
                <x-candidate-card :candidates="$jobPosting->offers()->get()" />
                {{-- @include('livewire/jobs/partials/candidates', [ 'candidates' => $jobPosting->offers()->get() ]) --}}
            </section>
            <section x-show="tab === 'hired'">
                <x-candidate-card :candidates="$jobPosting->hired()->get()" />
                {{-- @include('livewire/jobs/partials/candidates', [ 'candidates' => $jobPosting->hired()->get() ]) --}}
            </section>
            <section x-show="tab === 'favorites'">
                <x-candidate-card :candidates="$jobPosting->favoriteCandidates()->get()" />
                {{-- @include('livewire/jobs/partials/candidates', [ 'candidates' =>
                $jobPosting->favoriteCandidates()->get() ]) --}}
            </section>
            <section x-show="tab === 'discarded'">
                <x-candidate-card :candidates="$jobPosting->discardedCandidates()->get()" />
                {{-- @include('livewire/jobs/partials/candidates', [ 'candidates' =>
                $jobPosting->discardedCandidates()->get() ]) --}}
            </section>
        </div>
    @endif
</div>
