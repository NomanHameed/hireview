<div>
    <div class="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
        <h3 class="text-lg leading-6 font-medium text-gray-900">
            Job Postings
        </h3>
        <p class="mt-1 text-sm text-gray-500">
            You are using {{ $jobPostings->count() }} of 10 available job postings.
        </p>
    </div>

    <ul class="divide-y divide-gray-200">
        <ul>
            @forelse($jobPostings as $job)
                <li @if(!$loop->first)class="border-t border-gray-200"@endif>
                    <a href="{{ route('jobs.show', $job->id) }}" class="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out">
                        <div class="px-4 py-4 sm:px-6">
                            <div class="flex items-center justify-between">
                                <div class="text-sm leading-5 font-medium text-blue-900 truncate">
                                    {{ $job->title }}
                                </div>
                                <div class="ml-2 flex-shrink-0 flex">
                                    <span class="inline-flex text-sm leading-5 font-semibold text-gray-500">
                                        @if($job->published)
                                            <x-heroicon-o-badge-check class="-ml-1 mr-1.5 h-5 w-5 text-green-400" />
                                            Published
                                        @else
                                        <x-heroicon-o-exclamation-circle class="-ml-1 mr-1.5 h-5 w-5 text-red-400" />
                                            Unpublished
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="mt-2 sm:flex sm:justify-between">
                                <div class="sm:flex">
                                    <div class="mr-6 flex items-center text-sm leading-5 text-gray-500">
                                        <span class="text-blue-600 mr-1">{{ $job->candidates->count() }}</span> candidates
                                    </div>
                                    <div class="mr-6 mt-2 flex items-center text-sm leading-5 text-gray-500 sm:mt-0">
                                    <span class="text-indigo-600 mr-1">{{ $job->interviews()->count() }}</span> {{ $job->interviews()->count() === 1 ? 'Interview' : 'Interviews' }}
                                    </div>
                                    <div class="mt-2 flex items-center text-sm leading-5 text-gray-500 sm:mt-0">
                                        <span class="text-red-600 mr-1">{{ $job->discardedCandidates()->count() }}</span> discarded
                                    </div>
                                </div>
                                <div class="mt-2 flex items-center text-sm leading-5 text-gray-500 sm:mt-0">
                                <!-- Heroicon name: location-marker -->
                                <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd" />
                                </svg>
                                <span>
                                    {{ $job->location->name }}
                                </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            @empty
                <div class="rounded-md bg-yellow-50 p-4 m-4">
                    <div class="flex">
                        <div class="flex-shrink-0">
                            <x-heroicon-o-exclamation class="h-5 w-5 text-yellow-400" />
                        </div>
                        <div class="ml-3">
                            <h3 class="text-sm leading-5 font-bold text-yellow-800">
                                No job postings
                            </h3>
                            <div class="mt-2 text-sm leading-5 font-medium text-yellow-700">
                                <p>
                                    You have not created any job postings.
                                </p>
                            </div>
                            <div class="mt-4">
                                <div class="-mx-2 -my-1.5 ">
                                    <a href="#" class="px-2 py-1.5 flex rounded-md text-sm leading-5 font-bold text-yellow-800 hover:bg-yellow-100 focus:outline-none focus:bg-yellow-100 transition ease-in-out duration-150">
                                        Create a new job posting <x-heroicon-o-arrow-narrow-right class="ml-3 w-6 h-auto text-yellow-800" />
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforelse
        </ul>
    </ul>
</div>
