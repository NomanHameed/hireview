<x-jet-form-section submit="update">
    <x-slot name="title">
        {{ __('Location Branding') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Customize the branding of your location.') }}
    </x-slot>

    <x-slot name="form">
        <!-- Location Logo -->
        <div x-data="{logoName: null, logoPreview: null}" class="col-span-6 sm:col-span-4">
            <!-- Profile Photo File Input -->
            <input type="file" class="hidden"
                        wire:model="logo"
                        x-ref="logo"
                        x-on:change="
                                logoName = $refs.logo.files[0].name;
                                const reader = new FileReader();
                                reader.onload = (e) => {
                                    logoPreview = e.target.result;
                                };
                                reader.readAsDataURL($refs.logo.files[0]);
                        " />

            <!-- Current Location Logo -->
            <div class="mt-2" x-show="! logoPreview" x-cloak>
                <img src="{{ $this->team->logo_url }}" alt="{{ $this->team->name }}" class="object-cover">
            </div>

            <!-- New Location Logo Preview -->
            <div class="mt-2" x-show="logoPreview">
                <span class="block rounded-full w-20 h-20"
                        x-bind:style="'background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url(\'' + logoPreview + '\');'">
                </span>
            </div>

            <x-jet-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.logo.click()">
                {{ __('Select A New Logo') }}
            </x-jet-secondary-button>

            @if ($this->team->logo_path)
                <x-jet-secondary-button type="button" class="mt-2" wire:click="deleteLogo">
                    {{ __('Remove Logo') }}
                </x-jet-secondary-button>
            @endif

            <x-jet-input-error for="logo" class="mt-2" />
        </div>
        <!-- Location Color -->
        <div class="col-span-6">
            <x-color-picker wire:model="color" />
        </div>
    </x-slot>

    @if (Gate::check('update', $team))
        <x-slot name="actions">
            <x-jet-action-message class="mr-3" on="saved">
                {{ __('Saved.') }}
            </x-jet-action-message>

            <x-jet-button>
                {{ __('Save') }}
            </x-jet-button>
        </x-slot>
    @endif
</x-jet-form-section>
