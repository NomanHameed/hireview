<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Location Settings') }}
        </h2>
    </x-slot>

    <div>
        @livewire('teams.update-team-name-form', ['team' => $team])

        @if (Gate::check('update', $team))
            <x-jet-section-border/>
            <livewire:locations.branding :team="$team"/>
        @endif

        @livewire('teams.team-member-manager', ['team' => $team])

        @if (Gate::check('delete', $team) && ! $team->personal_team)
            <x-jet-section-border />

            <div class="mt-10 sm:mt-0">
                @livewire('teams.delete-team-form', ['team' => $team])
            </div>
        @endif
    </div>
</x-app-layout>
