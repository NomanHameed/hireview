<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    @livewireStyles
</head>

<body class="antialiased font-sans bg-gray-200">
    <x-jet-banner />
    <x-notification />
    <div class="min-h-screen bg-gray-100">

        <header x-data="{ open: false }" class="pb-24 bg-gradient-to-r from-light-blue-800 to-cyan-600">
            @livewire('navigation-menu')
        </header>

        <!-- Page Content -->
        <main class="-mt-24 pb-8">
            <div class="max-w-3xl mx-auto px-4 sm:px-6 lg:max-w-7xl lg:px-8">
                {{ $slot }}
            </div>
        </main>

        <footer>
            <div class="max-w-3xl mx-auto px-4 sm:px-6 lg:px-8 lg:max-w-7xl">
                <div class="border-t border-gray-200 py-8 text-sm text-gray-500 text-center sm:text-left"><span
                        class="block sm:inline">© 2021 TalentEQ.</span> <span class="block sm:inline">All rights
                        reserved.</span></div>
            </div>
        </footer>
    </div>

    @stack('modals')

    @livewireScripts

    @stack('scripts')

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
</body>

</html>
