<x-app-layout>
    @livewire('settings.subscription')
    <x-jet-section-border />
    @livewire('settings.payment-method')
    <x-jet-section-border />
    @livewire('settings.upcoming-payment')
    <x-jet-section-border />
    @livewire('settings.invoices')
    <x-jet-section-border />
    @livewire('settings.domains')
</x-app-layout>
