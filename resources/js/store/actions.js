let actions = {
    fetchJob({commit}) {
        axios.get(`/api/jobs/1`)
            .then(res => {
                console.log(res)
                commit('FETCH_JOB', res.data)
            }).catch(err => {
                console.log(err)
            })
    },
    submitApplication({commit}, candidate) {
        axios.post('/api/jobs/1/applications', candidate)
            .then(res => {
                commit('SUBMIT_APPLICATION', res.data)
            }).catch(err => {
                console.log(err)
            })
    }
}

export default actions
