let mutations = {
    FETCH_JOB(state, job) {
        return state.job = job
    },
    SUBMIT_APPLICATION(state, application) {
        state.applications.unshift(application)
    },
}

export default mutations
