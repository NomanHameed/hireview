require('./bootstrap');

import Vue from 'vue';
import store from './store/index'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import VueTelInput from 'vue-tel-input';
import 'vue-tel-input/dist/vue-tel-input.css';
const globalOptions = {
    mode: 'auto',
};

Vue.use(VueTelInput, globalOptions);

Vue.use(VueToast);
Vue.component('form-component', require('./components/FormComponent.vue').default);
Vue.component('application-component', require('./components/ApplicationComponent.vue').default);
Vue.component('record-component', require('./components/RecordComponent.vue').default);
import VueTailwindModal from "vue-tailwind-modal"
Vue.component("VueTailwindModal", VueTailwindModal)


const app = new Vue({
    el: '#app',
    store
});
