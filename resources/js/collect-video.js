/* eslint-disable */
import 'video.js/dist/video-js.min.css';
import videojs from 'video.js';
window.videojs = videojs;
import 'webrtc-adapter';
import RecordRTC from 'recordrtc';
window.RecordRTC = RecordRTC;
// register videojs-record plugin
import 'videojs-record/dist/css/videojs.record.css';
import Record from 'videojs-record/dist/videojs.record.js';
window.Record = Record;
let player;
const options = {
    controls: true,
    autoplay: false,
    fluid: true,
    responsive: true,
    loop: false,
    width: 320,
    height: 240,
    bigPlayButton: true,
    disablePictureInPicture: true,
    controlBar: {
        pictureInPictureToggle: false,
        volumePanel: {
            inline: true
        },
        playToggle: {
            show: true
        }
    },
    plugins: {
        // configure videojs-record plugin
        record: {
            audio: true,
            video: true,
            maxLength: 10,
            debug: true,
            pip: false,
        }
    }
};

// create player
player = videojs('myVideo', options, () => {
    // print version information at startup
    const msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ' and recordrtc ' + RecordRTC.version;
    videojs.log(msg);
});

// device is ready
player.on('deviceReady', () => {
    console.log('device is ready!');
});

// user clicked the record button and started recording
player.on('startRecord', () => {
    console.log('started recording!');
});

// user completed recording and stream is available
player.on('finishRecord', () => {
    // the blob object contains the recorded data that
    // can be downloaded by the user, stored on server etc.
    console.log('finished recording: ', player.recordedData);
});

// error handler
player.on('error', (element, error) => {
    console.error(error);
});

player.on('deviceError', () => {
    console.error(player.deviceErrorCode);
});

window.player = player;
