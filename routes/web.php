<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/apply/{jobPosting:slug}', \App\Http\Controllers\DisplayApplication::class)->name('application');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/dashboard', \App\Http\Livewire\Dashboard::class)->name('dashboard');

    Route::get('/jobs', \App\Http\Livewire\Jobs\JobIndex::class)->name('job-index');
    Route::get('/jobs/{jobPosting}', \App\Http\Livewire\Jobs\ShowJob::class)->name('jobs.show');

    Route::get('/candidates', \App\Http\Livewire\Candidates::class)->name('candidates');

    Route::get('/settings', '\App\Http\Controllers\SettingsController@index')->name('settings');
});
