<?php

namespace Database\Factories;

use App\Models\Application;
use Illuminate\Database\Eloquent\Factories\Factory;

class ApplicationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Application::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'candidate_id' => \App\Models\Candidate::all()->random()->id,
            'job_posting_id' => \App\Models\JobPosting::all()->random()->id,
            'is_favorite' => $this->faker->boolean(),
            'status' => $this->faker->randomElement(['applied','interview']),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'deleted_at' => $this->faker->boolean(20) ? \Carbon\Carbon::now()->addDay() : null,
        ];
    }
}
