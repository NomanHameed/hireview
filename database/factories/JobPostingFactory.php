<?php

namespace Database\Factories;

use App\Models\JobPosting;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class JobPostingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JobPosting::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $jobTitle = $this->faker->jobTitle;
        return [
            'title' => $jobTitle,
            'slug' => Str::slug($jobTitle),
            'description' => $this->faker->sentence(),
            'require_video' => true,
            'require_resume' => true,
            'location_id' => 1,
        ];
    }
}
