<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_posting_candidate', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('candidate_id')->index();
            $table->unsignedInteger('job_posting_id')->index();
            $table->boolean('is_favorite')->default(0);
            $table->string('status')->default('applied');
            $table->string('video')->nullable();
            $table->string('resume')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
