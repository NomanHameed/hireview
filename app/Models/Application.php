<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Application extends Pivot
{
    use HasFactory;

    /**
     * The table associated with the pivot model.
     *
     * @var string
     */
    protected $table = 'job_posting_candidate';

    /**
     * Returns the stages for the hiring process.
     *
     * @return \Illuminate\Support\Collection
     */
    public function stages()
    {
        return collect([
            'applied' => 'Applied',
            'phone' => 'Phone screening',
            'interview' => 'Schedule for interview',
            'offer' => 'Make an offer',
            'hired' => 'Hire'
        ]);
    }

    /**
     * Determine the candidates next stage in the hiring process.
     *
     * @return string|null
     */
    public function nextStage()
    {
        return $this->stages()->after($this->currentStage()) ?? $this->currentStage();
    }

    /**
     * Get the candidate's current stage in the application process.
     *
     * @return string
     */
    public function currentStage()
    {
        return $this->stages()->get($this->status, 'applied');
    }

    public function advance($stage = null)
    {
        $this->status = $this->stages()->search($stage ?? $this->nextStage());
        $this->deleted_at = null;
        $this->save();

        return $this;
    }
}
