<?php

namespace App\Models;

use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Candidate extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'photo',
    ];

    public function applications()
    {
        return $this->hasMany(Application::class);
    }

    public function photoUrl($size = 25, $options = null)
    {
        if (is_string($options)) {
            $options = ['default' => $options];
        } elseif (!is_array($options)) {
            $options = [];
        }

        // Default is "mm" (Mystery man)
        $default = Arr::get($options, 'default', 'mm');

        return $this->photo
            ? Storage::disk('photos')->url($this->photo)
            : '//www.gravatar.com/avatar/' . md5(strtolower(trim($this->email))) . '?s='. $size . '&d='. urlencode($default);
    }

    public function isFavorite()
    {
        return $this->applications()->where('is_favorite', true)->count();
    }

    public function scopeInterviews()
    {
        return $this->whereHas('applications', function ($q) {
            $q->where('status', 'interview');
        });
    }
}
