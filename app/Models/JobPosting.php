<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class JobPosting extends Model
{
    use HasFactory;

    public $fillable = [
        'slug',
        'title',
        'description',
        'require_video',
        'require_resume',
        'location_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['published_at'];

    protected $casts = [
        'require_resume' => 'boolean',
        'require_video' => 'boolean',
        'published' => 'boolean'
    ];

    /**
     * Get the location this job posting belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(Team::class, 'location_id', 'id', 'teams');
    }

    /**
     * Get all of candidates for this job posting.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function candidates()
    {
        return $this->belongsToMany(Candidate::class, 'job_posting_candidate')
            ->using('App\Models\Application')
            ->as('application')
            ->withPivot('id', 'video', 'is_favorite', 'status', 'deleted_at')
            ->withTimestamps();
    }

    public function favoriteCandidates()
    {
        return $this->candidates()->wherePivot('is_favorite', true)->wherePivotNull('deleted_at');
    }

    public function discardedCandidates()
    {
        return $this->candidates()->whereNotNull('deleted_at');
    }

    public function interviews()
    {
        return $this->candidates()->wherePivot('status', 'interview')->wherePivotNull('deleted_at');
    }

    public function applied()
    {
        return $this->candidates()->wherePivot('status', 'applied')->wherePivotNull('deleted_at');
    }

    public function phoneScreenings()
    {
        return $this->candidates()->wherePivot('status', 'phone')->wherePivotNull('deleted_at');
    }

    public function offers()
    {
        return $this->candidates()->wherePivot('status', 'offer')->wherePivotNull('deleted_at');
    }

    public function hired()
    {
        return $this->candidates()->wherePivot('status', 'hired')->wherePivotNull('deleted_at');
    }

    public function scopeIsPublished($query)
    {
        return $query
            ->whereNotNull('published')
            ->where('published', true)
            ->whereNotNull('published_at')
            ->where('published_at', '<', Carbon::now())
        ;
    }

    public function applicationUrl()
    {
        return url('apply/' . $this->slug);
    }

    /**
     * Used to test if a certain user has permission to edit post,
     * returns TRUE if the user is the owner or has other posts access.
     * @param  BackendUser $user
     * @return bool
     */
    public function canEdit(User $user)
    {
        return ($this->user_id == $user->id) || $user->hasAnyAccess(['rainlab.blog.access_other_posts']);
    }

    public function publish()
    {
        $this->published = true;
        $this->published_at = now();
        $this->save();
    }

    public function unpublish()
    {
        $this->published = false;
        $this->save();
    }
}
