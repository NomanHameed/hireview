<?php

namespace App\Http\Controllers\Api;

use App\Models\Candidate;
use App\Models\JobPosting;
use App\Models\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
/**
 * Class StoreApplication
 * @package App\Http\Controllers\Api
 */
class StoreApplication extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param JobPosting $jobPosting
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(JobPosting $jobPosting, Request $request)
    {
        $resume = $request->file('resume')->store('/', 'resumes');
        $photo = $request->file('photo')->store('/', 'photos');

        if ($request->hasFile('video')) {
            $video = cloudinary()->upload($request->file('video')->getRealPath(), [
                'resource_type' => 'video',
                'folder' => 'uploads/videos',
                'transformation' => [
                    'quality' => 'auto',
                    'fetch_format' => 'mp4',
                ]
            ])->getSecurePath();
        }

        $candidate = Candidate::updateOrCreate([
            'email' => $request->email
        ], [
            'name'  => $request->full_name,
            'phone' => $request->phone,
            'photo' => $photo,
        ]);

        $application = Application::create([
            'candidate_id' => $candidate->id,
            'job_posting_id' => $jobPosting->id,
            'resume' => $resume,
            'video' => $video ? $video : null,
        ]);

        return response()->json([
            'status' => 'Success.',
            'result' => [
                'application' => [
                    'id' => $application->id,
                    'candidate' => $candidate
                ],
            ]
        ], 200);
    }

    protected function findOrCreateCandidate()
    {
        return Candidate::updateOrCreate([
            'email' => $this->email
        ], [
            'name'  => $this->fullName,
            'phone' => $this->phone
        ]);
    }
}
