<?php

namespace App\Http\Controllers\Api;

use App\Models\JobPosting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetSingleJobDetails extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $data = JobPosting::isPublished()
            ->where('slug', $request->slug)
            ->with('location')
            ->first();

        return response()->json($data, 200);
    }
}
