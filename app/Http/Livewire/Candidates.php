<?php

namespace App\Http\Livewire;

use App\Models\Candidate;
use Livewire\Component;

class Candidates extends Component
{
    public function render()
    {
        return view('livewire.candidates', ['candidates' => Candidate::simplePaginate(8)]);
    }
}
