<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Candidate;
use App\Models\JobPosting;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Storage;

class Application extends Component
{
    use WithFileUploads;

    public $demo = false;

    /** @var integer */
    public $step = 1;

    /** @var JobPosting */
    public $jobPosting;

    /** @var Candidate */
    private $candidate;

    /** @var TemporaryUploadedFile */
    public $photo;

    /** @var string */
    public $fullName = '';

    /** @var string */
    public $email = '';

    /** @var string */
    public $phone = '';

    /** @var string */
    public $profileUrl = '';

    /** @var TemporaryUploadedFile */
    public $video;

    /** @var TemporaryUploadedFile */
    public $resume;

    public $rules = [
        'fullName' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'resume' => 'sometimes|nullable|file',
        'photo' => 'sometimes|nullable|image',
        'video' => 'sometimes|nullable|mimes:mp4,ogx,oga,ogv,ogg,webm,qt'
    ];

    // public function updated($propertyName)
    // {
    //     $this->validateOnly($propertyName);
    // }

    public function mount(JobPosting $jobPosting)
    {
        if ($this->demo) {
            return;
        }
        $this->jobPosting = $jobPosting;

        // if ($this->candidate = auth()->guard('candidate')->user()) {
        //     $this->fullName = $this->candidate->name;
        //     $this->email = $this->candidate->email;
        //     $this->phone = $this->candidate->phone;
        //     $this->photo = $this->candidate->photo;
        //     $this->step = 1;
        // }
    }

    public function submit()
    {
        $this->validate();

        if ($this->demo) {
            return;
        }

        $candidate = $this->findOrCreateCandidate();

        if ($this->photo) {
            // Store in the "photos" directory of a configured "s3" bucket.
            $candidate->photo = $this->photo->store('/', 'photos');
        }

        $candidate->save();

        if ($this->video) {
            // Store in the "videos" directory of a configured "s3" bucket.
            $videoPath = $this->video->store('/', 'videos');
        }

        if ($this->resume) {
            // Store in the "resumes" directory of a configured "s3" bucket
            $resumePath = $this->resume->store('/', 'resumes');
        }

        $this->jobPosting->candidates()->attach($candidate, [
            'video' => isset($videoPath) ? $videoPath : null,
            'resume' => isset($resumePath) ? $resumePath: null
        ]);
    }

    protected function findOrCreateCandidate()
    {
        if ($this->candidate) {
            return $this->candidate;
        }

        return Candidate::updateOrCreate([
            'email' => $this->email
        ], [
            'name'  => $this->fullName,
            'phone' => $this->phone
        ]);
    }

    public function render()
    {
        return view('livewire.application', [
            'jobPosting' => $this->jobPosting,
            'candidate' => $this->candidate,
            'step' => $this->step
        ])->layout('layouts.guest');
    }

    public function nextStep()
    {
        $rule = array_keys($this->rules)[$this->step - 1];
        $this->validate([$rule => $this->rules[$rule]]);
        $this->step = $this->step + 1;
            $this->dispatchBrowserEvent('gotostep', $this->step);
    }

    public function previousStep()
    {
        $this->step = $this->step - 1;
        $this->dispatchBrowserEvent('gotostep', $this->step);
    }

    public function resetVideo()
    {
        if (!$this->video) {
            return;
        }
        debug($this->video);
        $this->video = null;
        $this->dispatchBrowserEvent('reset-video-player');
    }

    public function getUploadedVideoUrl()
    {
        if ($this->video) {
            return $this->video->temporaryUrl();
        }
    }
}
