<?php

namespace App\Http\Livewire\Settings;

use Livewire\Component;

class Invoices extends Component
{
    public function render()
    {
        return view('livewire.settings.invoices');
    }
}
