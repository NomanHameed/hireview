<?php

namespace App\Http\Livewire\Settings;

use Livewire\Component;

class Subscription extends Component
{
    public function render()
    {
        return view('livewire.settings.subscription');
    }
}
