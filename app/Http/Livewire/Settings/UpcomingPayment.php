<?php

namespace App\Http\Livewire\Settings;

use Livewire\Component;

class UpcomingPayment extends Component
{
    public function render()
    {
        return view('livewire.settings.upcoming-payment');
    }
}
