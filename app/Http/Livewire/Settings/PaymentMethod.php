<?php

namespace App\Http\Livewire\Settings;

use Livewire\Component;

class PaymentMethod extends Component
{
    public function render()
    {
        return view('livewire.settings.payment-method');
    }
}
