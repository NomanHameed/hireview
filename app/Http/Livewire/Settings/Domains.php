<?php

namespace App\Http\Livewire\Settings;

use Livewire\Component;

class Domains extends Component
{
    public function render()
    {
        return view('livewire.settings.domains');
    }
}
