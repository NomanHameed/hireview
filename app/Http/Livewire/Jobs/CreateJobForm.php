<?php

namespace App\Http\Livewire\Jobs;

use Livewire\Component;
use App\Models\JobPosting;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class CreateJobForm extends Component
{
    /** @var string */
    public $title = '';

    /** @var string */
    public $description = '';

    /** @var boolean */
    public $requireVideo = true;

    /** @var boolean */
    public $requireResume = true;

    public $jobPosting;

    public $isOpen;

    protected $listeners = ['closeModal'];

    public function mount()
    {
        if (!$this->jobPosting) {
            return;
        }

        $this->title = $this->jobPosting->title;
        $this->description = $this->jobPosting->description;
        $this->requireVideo = $this->jobPosting->require_video;
        $this->requireResume = $this->jobPosting->require_resume;
    }

    public function createJob()
    {
        $this->validate([
            'title' => 'required',
            'description' => 'required',
            'requireVideo' => 'required|boolean',
            'requireResume' => 'required|boolean',
        ]);

        JobPosting::create([
            'slug' => Str::slug($this->title),
            'title' => $this->title,
            'description' => $this->description,
            'require_video' => $this->requireVideo,
            'require_resume' => $this->requireVideo,
            'location_id' => Auth::user()->current_team_id
        ]);

        redirect(route('job-index'));
    }
    public function render()
    {
        return view('livewire.jobs.create-job-form');
    }

    public function editJob()
    {
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }
}
