<?php

namespace App\Http\Livewire\Jobs;

use App\Models\JobPosting;
use Livewire\Component;

class JobList extends Component
{
    public function render()
    {
        return view('livewire.jobs.job-list', [ 'jobPostings' => JobPosting::simplePaginate(6) ]);
    }
}
