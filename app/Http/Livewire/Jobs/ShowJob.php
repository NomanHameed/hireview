<?php

namespace App\Http\Livewire\Jobs;

use App\Models\Application;
use Livewire\Component;
use App\Models\JobPosting;

class ShowJob extends Component
{
    /** @var JobPosting */
    public $jobPosting;

    protected $listeners = [
        'applicationAdvanced' => '$refresh',
        'favorite' => '$refresh',
        'closeModal' => 'closeModal'
    ];

    public function mount(JobPosting $jobPosting)
    {
        $this->jobPosting = $jobPosting;
    }

    public function advance($applicationId, $stage = null)
    {
        $application = Application::findOrFail($applicationId);
        $application = $application->advance($stage);

        $this->emit('applicationAdvanced');
        $this->dispatchBrowserEvent('notify', 'Moved candidate to ' . strtolower($application->currentStage()) . ' stage!');
    }

    public function favorite($applicationId)
    {
        $application = Application::findOrFail($applicationId);
        $application->is_favorite = true;
        $application->deleted_at = null;
        $application->save();

        $this->emit('favorite');
        $this->dispatchBrowserEvent('notify', 'Candidate added to favorites!');
    }

    public function discard($applicationId)
    {
        $application = Application::findOrFail($applicationId);
        $application->deleted_at = now();
        $application->save();

        $this->emit('discard');
        $this->dispatchBrowserEvent('notify', 'Discarded candidate\'s application!');
    }

    public function render()
    {
        return view('livewire.jobs.show-job');
    }

    public $editingJob = false;

    public function editJob()
    {
        $this->editingJob = true;
    }

    public function closeModal()
    {
        $this->editingJob = false;
    }

    public function publish()
    {
        $this->jobPosting->publish();
        $this->dispatchBrowserEvent('notify', 'Successfully published job posting!');
    }

    public function unpublish()
    {
        $this->jobPosting->unpublish();
        $this->dispatchBrowserEvent('notify', 'Successfully unpublished job posting!');
    }
}
