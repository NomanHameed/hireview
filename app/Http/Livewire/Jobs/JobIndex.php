<?php

namespace App\Http\Livewire\Jobs;

use App\Models\JobPosting;
use Livewire\Component;

class JobIndex extends Component
{
    public function render()
    {
        return view('livewire.jobs.index');
    }
}
