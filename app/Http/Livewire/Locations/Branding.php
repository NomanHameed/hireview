<?php

namespace App\Http\Livewire\Locations;

use App\Models\Team;
use Livewire\Component;
use Livewire\WithFileUploads;

class Branding extends Component
{
    use WithFileUploads;

    public $team;
    public $logo;
    public $color;

    public function mount(Team $team)
    {
        $this->team = $team;
        $this->color = $this->team->color ?? '#2196F3';
    }

    public function update()
    {
        if ($this->logo) {
            $this->team->updateLogo($this->logo);
        }

        $this->team->forceFill([
            'color' => $this->color
        ])->save();

        if (isset($this->logo)) {
            return redirect()->route('teams.show', ['team' => $this->team]);
        }

        $this->emit('saved');

        $this->emit('refresh-navigation-menu');
    }

    /**
     * Delete location's logo.
     *
     * @return void
     */
    public function deleteLogo()
    {
        $this->team->deleteLogo();

        $this->emit('refresh-navigation-menu');
    }

    public function render()
    {
        return view('livewire.locations.branding');
    }
}
