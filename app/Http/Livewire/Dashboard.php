<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Candidate;
use App\Models\JobPosting;

class Dashboard extends Component
{
    public function render()
    {
        return view('livewire.dashboard', [
            'totalPublishedJobs' => JobPosting::isPublished()->count(),
            'totalCandidates' => Candidate::all()->count(),
            'totalInterviews' => Candidate::interviews()->count(),
        ]);
    }
}
