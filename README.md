<p align="center">TalentEQ</p>

## About TalentEQ

Hire your next superstar. TalentEQ leverages video to quickly identify top-tier applicants and make the best hiring decisions possible.

## Setup Instructions

1. Clone this repository
2. Run `composer install && npm install` to install dependencies
3. Run `cp .env.example .env` to copy the example env file
4. Run `php artisan key:generate` to generate a new application key
5. Create a new database named `hireview`
6. Visit `hireview.test` if you are using Valet

## Foundation library

TalentEQ uses Laravel as a foundation PHP framework. Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to [larry@larrybarker.me](mailto:larry@larrybarker.me). All security vulnerabilities will be promptly addressed.

## License

Wheelhouse is [proprietary software](https://en.wikipedia.org/wiki/Proprietary_software).
